import React, { Component } from 'react';

class Counter extends Component {
  // componentDidUpdate(previousProps, previousState) {
  //   console.log('previousProps', previousProps);
  //   console.log('previousState', previousState);
  // }

  // componentWillUnmount() {
  //   console.log('Counter - Unmount');
  // }

  render() {
    // console.log('Counter - Rendered');
    const { counter, onIncrement, onDecrement, onDelete } = this.props;
    const { id, value } = counter;
    return (
      <div className="row">
        <div className="col-1">
          <span className={this.getBadgeClasses()}>{this.formatCount()}</span>
        </div>
        <div className="col">
          <button
            onClick={() => onIncrement(counter)}
            className="btn btn-secondary btn-sm"
          >
          +
          </button>
          <button
            onClick={() => onDecrement(counter)}
            className="btn btn-secondary btn-sm m-2"
            disabled={value === 0}
          >
            -
          </button>
          <button 
            className="btn btn-danger btn-sm"
            onClick={() => onDelete(id)}
          >
          X
          </button>
        </div>
      </div>
    );
  }

  getBadgeClasses() {
    let classes = 'badge m-2 badge-';
    classes += (this.props.counter.value === 0) ? 'warning' : 'primary';
    return classes;
  }

  formatCount() {
    const { value } = this.props.counter;
    return value === 0 ? 'Zero' : value;
  }
}

export default Counter;
